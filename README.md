# 3D printing stash

Keeping my important 3D printing stuff here for safekeeping

These are the configuration files for Marlin 2.0 for an ANET A8, using an E3D Hemera for the extruder and a BLTouch for the Z probe.

oldConfiguration.h has the old configurations for the Titan Aero extruder with a reduced print bed due to a badly cut glass and also used a BLTouch Z probe.

## Useful links

### List of bracketed symbols for CURA

https://github.com/Ultimaker/Cura/blob/master/resources/definitions/fdmprinter.def.jsonhttps://github.com/Ultimaker/Cura/blob/master/resources/definitions/fdmprinter.def.json
